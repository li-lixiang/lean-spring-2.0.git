package com.gupaoedu.vip.spring.demo.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.gupaoedu.vip.spring.demo.service.IQueryService;
import com.gupaoedu.vip.spring.formework.annotation.GPService;
import lombok.extern.slf4j.Slf4j;

/**
 * 查询业务
 * @author Tom
 *
 */
@GPService
@Slf4j
public class QueryService implements IQueryService {

	/**
	 * 查询
	 */
	public String query(String name) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = sdf.format(new Date());
		String json = "name:\"" + name + "\",time:\"" + time + "\"";
		json = json.concat(" lilixiang homework");
		json = json.concat(" 李利祥作业");
		log.info("这是在业务方法中打印的：" + json);

		String view = "<!DOCTYPE html>\n" +
				"<html lang=\"zh-cn\">\n" +
				"<head>\n" +
				"    <meta charset=\"utf-8\">\n" +
				"    <title>李利祥作业</title>\n" +
				"</head><body><b>";
		view = view.concat(json);
		view = view.concat("</b></body></html>");
		return view;
	}

}
