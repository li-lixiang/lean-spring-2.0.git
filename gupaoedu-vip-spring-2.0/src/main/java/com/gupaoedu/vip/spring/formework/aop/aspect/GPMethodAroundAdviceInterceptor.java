/**
 * Copyright (C), 2015-2020, XXX有限公司
 * FileName: GPMethodAroundAdviceInterceptor
 * Author:   lilx
 * Date:     2020/12/5 14:46
 * Description: 环绕执行
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.gupaoedu.vip.spring.formework.aop.aspect;

import com.gupaoedu.vip.spring.formework.aop.intercept.GPMethodInterceptor;
import com.gupaoedu.vip.spring.formework.aop.intercept.GPMethodInvocation;

import java.lang.reflect.Method;

/**
 * 〈一句话功能简述〉<br> 
 * 〈环绕执行〉
 *
 * @author lilx
 * @create 2020/12/5
 * @since 1.0.0
 */
public class GPMethodAroundAdviceInterceptor extends GPAbstractAspectAdvice implements GPAdvice, GPMethodInterceptor {
    private GPJoinPoint joinPoint;

    public GPMethodAroundAdviceInterceptor(Method aspectMethod, Object aspectTarget) {
        super(aspectMethod, aspectTarget);
    }
    public void around(int i){
        if(i==0){
            System.out.println("GPMethodAroundAdviceInterceptor.around(...) start");
        }else{
            System.out.println("GPMethodAroundAdviceInterceptor.around(...) end");
        }
    }

    @Override
    public Object invoke(GPMethodInvocation mi) throws Throwable {
        //从被织入的代码中才能拿到，JoinPoint
        this.joinPoint = mi;
        around(0);
        Object retVal = mi.proceed();
        around(1);
        return retVal;
    }

}